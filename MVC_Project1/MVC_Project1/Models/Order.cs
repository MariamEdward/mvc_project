﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Project1.Models
{
    [Table("Order")]
    public class Order
    {
        public int ID { get; set; } //identity

        [ForeignKey("User")]
        public String UserID { get; set; } //user not customer
        public IdentityUser User { get; set; }

        public int OrderNumber { get; set; } //should be calculated

        //[ForeignKey("Payment")]
        //public int PaymentID { get; set; }
        //public Payment Payment { get; set; }

        //public DateTime ShipDate { get; set; }

        //public double SalesTax { get; set; } = 14;


        public double SalesTax { get; set; } //default
        public double Freight { get; set; }
        public TimeSpan TimeStamp { get; set; }
        public bool Fulfill { get; set; }
        public bool Paid { get; set; } //default false
        //public DateTime PaymentDate { get; set; }


        public virtual ICollection<OrderDetails> OrderDetails { get; set; }
    }
}