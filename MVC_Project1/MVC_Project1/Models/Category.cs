﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Project1.Models
{
    [Table("Category")]
    public class Category
    {
        //identity
        public int ID { get; set; }
        public string Name { get; set; }  //required
        public string Description { get; set; }
        public string Picture { get; set; }
        public bool Active { get; set; }
    }
}