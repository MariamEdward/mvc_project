﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Project1.Models
{
    [Table("Product")]
    public class Product
    {

        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; } //identity
        public string Name { get; set; } //required
        public string Description { get; set; }

        [ForeignKey("Category")]
        public int CategoryID { get; set; }
        public virtual Category Category { get; set; }

        public virtual ICollection<Item> Items { get; set; }

        //size and color can be calculated from list of items ?
        public string AvailableColors { get; set; }  // collection string??
        public string AvailableSizes { get; set; }  // coleection string?? 
        public bool ProductAvailable { get; set; } //default
        public bool ProductDiscount { get; set; } //default
    }
}