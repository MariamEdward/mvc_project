﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MVC_Project1.Models
{
    [Table("Item")]
    public class Item
    {
        public int ID { get; set; } // identity

        [ForeignKey("Product")]
        public int ProductID { get; set; }
        public Product Product { get; set; }

        public int Size { get; set; }  // string in product table 
        public string Color { get; set; }

        [Range(0.0, 100.0)]
        public double Discount { get; set; }
        public double Price { get; set; }
        public int UnitsInStock { get; set; } //can be calculated from items list in product property
        public string Picture { get; set; }

        [Range(1.0, 5.0)]
        public double Rank { get; set; }

        public string Note { get; set; }
    }
}